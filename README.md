# FlintFiller English

# FlintFiller

This project is created to automatically create Flint Frames from **English** text. This repo contains the code for
the srl-based approach for **English** texts. If you want to use the srl-based approach for **Dutch** texts, please go to [the FlintFiller-srl Dutch project](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl). 