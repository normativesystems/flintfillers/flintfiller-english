import spacy
import nltk
import pandas as pd
from pattern.en import parse
from ast import literal_eval
from collections import defaultdict


from preprocess_pattern_spacy_output import reformat_pattern_chunk
from preprocess_pattern_spacy_output import drop_non_matching_tokens
from preprocess_pattern_spacy_output import combine_pattern_spacy

# dependency parsing and pos tagging with spacy
nlp = spacy.load('en_core_web_sm')

df_large = pd.read_csv('../../../src/resource/to_be_labeled/test_set_to_be_labeled.csv')
df_spacy = df_large.copy()


df_pattern = pd.read_csv('test_sents_chunked.csv')


def pos_dep_tag_text_spacy(text_column: pd.Series) -> list:
    tagged_text = []
    for sentence in text_column:
        tagged_sentence = []
        spacy_doc = nlp(sentence)
        for token in spacy_doc:
            pos_dep_tagged_token = token.text, token.pos_, token.dep_
            tagged_sentence.append(pos_dep_tagged_token)
        tagged_text.append(tagged_sentence)
    return tagged_text

text_column = df_spacy['source_text']
tagged_text = pos_dep_tag_text_spacy(text_column)
print(tagged_text)

df_spacy['spacy_tags'] = tagged_text


# dependency tag to flint role rules
def get_actor(dependency: str) -> str:
    if dependency == 'nsubj' or dependency == 'obl:agent':
        return True

def get_object(dependency: str) -> str:
    if dependency == 'dobj' or dependency == 'nsubjpass':
        return True

def get_recipient(dependency: str) -> str:
    if dependency == 'dative':
        return True

def get_action(dependency: str) -> str:
    if dependency == 'ROOT' or dependency == 'ccomp' or dependency == 'xcomp':
        return True

def add_roles(pattern_list: list, spacy_list: list) -> dict:

    store_result = defaultdict(list)

    for pattern_lst, spacy_lst in zip(pattern_list, spacy_list):
        token = spacy_lst[0]
        pos = spacy_lst[1]
        dep = spacy_lst[2]
        chunk = pattern_lst[2]

        if get_actor(dep):
            role = "Actor"
        elif get_object(dep):
            role = "Object"
        elif get_recipient(dep):
            role = "Recipient"
        elif get_action(dep):
            role = "Action"
        else:
            role = ""
        store_result[chunk].append([token,pos,dep,role])
    return store_result

def roles_to_list_for_evaluation(dct: dict):
    punctuation_tokens = [",", ".", ":", "(", ")", ";"]
    sentence_lst_token = []
    sentence_lst_role = []
    sentence_token_role_tuple = []
    for chunk_tag, list_of_lists in dct.items():
        most_frequent_role = [x[3] for x in list_of_lists if not x[3] == '']
        if most_frequent_role:
            most_frequent_role = most_frequent_role[0]
        else:
            most_frequent_role = "O"
        for token, pos, dep, role in list_of_lists:
            if token in punctuation_tokens:
                most_frequent_role = "O"
            sentence_lst_token.append(token)
            sentence_lst_role.append(most_frequent_role)
            sentence_token_role_tuple.append((token, most_frequent_role))
        most_frequent_role = ""
    return sentence_lst_token, sentence_lst_role, sentence_token_role_tuple

def add_phrase_rule(original_sentence, pattern_sentence, spacy_sentence):
    reformatted_pattern_chunk = reformat_pattern_chunk(pattern_sentence)
    equal_tokens_list = drop_non_matching_tokens(spacy_sentence, reformatted_pattern_chunk)
    equal_length_pattern = combine_pattern_spacy(equal_tokens_list, spacy_sentence)
    chunk_with_roles_dict = add_roles(equal_length_pattern, spacy_sentence)

    # write the output to a file for evaluation
    sentence_lst_token, sentence_lst_role, sentence_token_role_tuple = roles_to_list_for_evaluation(chunk_with_roles_dict)
    return original_sentence, sentence_lst_token, sentence_lst_role, sentence_token_role_tuple

# main function
def main(pattern_df, spacy_df):

    # store output
    sentence_ids = []
    original_sentences = []
    labeled_sentences_token = []
    labeled_sentences_role = []
    labeled_sentences_tuple = []

    for sent_id, original_sent, pattern_sent, spacy_sent in zip(list(pattern_df['sentence_id']),
                                                                list(pattern_df['source_text']),
                                                                pattern_df['chunks'],
                                                                spacy_df['spacy_tags']):
        pattern_sent = literal_eval(pattern_sent)
        spacy_sent = spacy_sent

        original_sentence, tokens, roles, labeled_sentence = add_phrase_rule(original_sent, pattern_sent, spacy_sent)

        sentence_ids.append(sent_id)
        original_sentences.append(original_sentence)
        labeled_sentences_token.append(tokens)
        labeled_sentences_role.append(roles)
        labeled_sentences_tuple.append(labeled_sentence)

    return labeled_sentences_tuple

predicted_labels = main(df_pattern, df_spacy)

print(predicted_labels)

df_rlb = df_large.copy()
df_rlb.drop('tokens', axis=1, inplace=True)
df_rlb['predicted_labels'] = predicted_labels

# df_rlb.to_csv('../../../src/resource/labeled_results/rulebased_predictions.csv', index=False)
df_rlb.to_csv('../../../src/resource/labeled_results/rulebased_predictions.csv', index=False)








