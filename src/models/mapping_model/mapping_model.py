import argparse
import pathlib
import spacy
from transformer_srl import dataset_readers, models, predictors
import re
import pandas as pd
import csv

directory = pathlib.Path(__file__).parent

# The mapping from thematic roles from VerbAtlas to Flint roles
thematic_to_flint_mapping = {'Agent': 'Actor',
                             'Patient': 'Object', 'Theme': 'Object', 'Topic' : 'Object', 'Asset' : 'Object',
                             'Beneficiary': 'Recipient', 'Recipient': 'Recipient',
                             'V': 'Action'}

def create_mapping_dict():
    """Reads the tsv file and returns the mapping of PropBank to thematic VerbAtlas roles."""
    with open('src/models/mapping_model/pb2va.tsv') as f:
        mapping_lines = f.readlines()[1:]

    frame_mappings = {}

    for line in mapping_lines:
        parts = line.split()
        frame_key = parts[0].split('>')[0]
        value = {}
        for part in parts[1:]:
            k, v = part.split('>')
            value[k] = v
        frame_mappings[frame_key] = value

    return frame_mappings

def load_models():
    """Loads dependency tagging model and SRL model."""
    nlp = spacy.load('en_core_web_sm')
    predictor = predictors.SrlTransformersPredictor.from_path('src/models/mapping_model/srl_bert_base_conll2012.tar.gz')
    return nlp, predictor

def get_sentence_root(nlp, sentence):
    """Takes a sentence and returns the root of the sentence."""
    doc = nlp(sentence)
    root = None
    for token in doc:
        if token.head == token:
            root = token
            break

    return root

def get_propbank_tags(nlp, predictor, sent):
    """Takes a sentence and a predictor and returns the propbank tags for the sentence root."""
    root = str(get_sentence_root(nlp, sent))

    prediction = predictor.predict(sentence=sent)

    # get list with O tags for each word in the sentence, in case the root is not a verb from PropBank's tags
    words = sent.split()
    tags = ['O' for word in words]

    frame = ''

    for verb in prediction['verbs']:
        if verb['verb'] == root:
            tags = verb['tags']
            frame = verb['frame']
    return tags, frame

def get_thematic_tags(prop_tags,frame,mapping_dict):
    """Maps the PropBank roles to thematic roles from VerbAtlas.

    Args:
    - prop_tags:    list with PropBank tags for the sequence
    - frame:        relevant predicate sense/PropBank frame for the sequence
    - mapping_dict: the dictionary with the mapping from propbank to verbatlas

    Returns:
    - thematic_tags: list with the thematic tags for the sequence
    """
    if frame in mapping_dict:
        mapping = mapping_dict[frame]

        # remove BIO-tagging
        stripped_tags = []
        for tag in prop_tags:
            stripped_tag = re.sub('I-ARG|^B-ARG', 'A', tag)
            stripped_tags.append(stripped_tag)

        # convert to thematic
        thematic_tags = []
        for tag in stripped_tags:
            if tag == 'B-V':  # leave VERB tags in tact
                thematic_tags.append('V')
            else:  # map all other tags according to dict or map to O
                mapped_tag = mapping.get(tag, 'O')
                thematic_tags.append(mapped_tag)
    else:
        thematic_tags = prop_tags


    return thematic_tags

def get_flint_tags(thematic_tags, flint_mapping):
    flint_tags = []
    for tag in thematic_tags:
        mapped_tag = flint_mapping.get(tag, 'O')
        flint_tags.append(mapped_tag)
    return flint_tags


def map_sequence(sequence):
    """
    Can be used to label a single text sequence, as opposed to multiple sequences (in a csv file or a dataset object).
    - If you want to label text sequences in a csv file, use map_sentences().
    - If you want to label text sequences in a dataset object, use label_encoded_set().

    Args:
        sequence: str
    """
    mapping_dict = create_mapping_dict()
    nlp, predictor = load_models()
    prop_tags, frame = get_propbank_tags(nlp, predictor, sequence)
    thematic_tags = get_thematic_tags(prop_tags, frame, mapping_dict)
    flint_tags = get_flint_tags(thematic_tags, thematic_to_flint_mapping)

    print(flint_tags)

def map_sentences(data_path, output_path):
    """
    Can be used to label text sequences in a csv file
    - If you want to label text sequences in dataset object, use map_encoded_set().
    - If you want to label a single text sequence (str), use map_sequence().

    Args:
        data_path: the path to the data file to be labeled
        output_path: the path for the output file
    """
    df = pd.read_csv(data_path, sep=",")
    mapping_dict = create_mapping_dict()
    nlp, predictor = load_models()

    with open(output_path, 'w', encoding='UTF8', newline='') as f:
        # create the csv writer
        writer = csv.writer(f)

        writer.writerow(['sentence_id', 'source_text', 'srl_tags', 'predicted_labels'])

        for index, row in df.iterrows():
            prop_tags, frame = get_propbank_tags(nlp, predictor,row['source_text'])
            thematic_tags = get_thematic_tags(prop_tags,frame,mapping_dict)
            flint_tags = get_flint_tags(thematic_tags,thematic_to_flint_mapping)

            writer.writerow([row['sentence_id'],
                            row['source_text'],
                            row['srl_tags'],
                            flint_tags])


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data',
                        help='data path')
    parser.add_argument('-s', '--seq',
                        help='sequence')
    parser.add_argument('-o', '--output',
                        help='save location')
    args = parser.parse_args()

    return args

if __name__ == '__main__':
    args = parse_commandline_arguments()
    if args.data:
        if args.data.endswith('.csv'):
            map_sentences(args.data, args.output)
    elif args.seq:
        map_sequence(args.seq)
    else:
        print('please enter the path of your data or a sequence')
