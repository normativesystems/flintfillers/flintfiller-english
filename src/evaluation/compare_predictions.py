import pandas as pd


def format_bert_predictions(pred_output):
    # remove CLS and SEP token at start and end of list
    pred_output = pred_output[1:-1]

    # concatenate the tokens that start with ## to their preceding token
    cleaned_pred_output = []
    for label in pred_output:
        label = list(label)
        if not label[0].startswith('##'):
            cleaned_pred_output.append(label)
        else:
            cleaned_pred_output[-1][0] += label[0].strip('##')

    # Map the different usages of arguments
    label_mapping = {'ACTOR': 'Actor',
                     'OBJ': 'Object',
                     'REC': 'Recipient',
                     'V': 'Action',
                     'O': 'O'}

    for label in cleaned_pred_output:
        label[1] = label_mapping[label[1]]

    cleaned_pred_output = [tuple(label) for label in cleaned_pred_output]

    return cleaned_pred_output


# load all predictions dataframes
df_rlb = pd.read_csv("../resource/labeled_results/rulebased_predictions.csv", delimiter=',',
                     converters={'predicted_labels': eval, 'srl_tags': eval})
df_mapping = pd.read_csv("../resource/labeled_results/mapping_predictions.csv", delimiter=',',
                         converters={'predicted_labels': eval})
df_bert = pd.read_csv("../resource/labeled_results/bert_predictions.csv", delimiter=',',
                      converters={'predicted_labels': eval})
df_legal = pd.read_csv("../resource/labeled_results/legalbert_predictions.csv", delimiter=',',
                       converters={'predicted_labels': eval})
df_eurlex = pd.read_csv("../resource/labeled_results/eurlex_predictions.csv", delimiter=',',
                        converters={'predicted_labels': eval})
df_spanbert = pd.read_csv("../resource/labeled_results/spanbert_predictions.csv", delimiter=',',
                          converters={'predicted_labels': eval})
df_multilingual = pd.read_csv("../resource/labeled_results/multilingual_predictions.csv", delimiter=',',
                              converters={'predicted_labels': eval})

# instantiate dataframe for comparison
df_comparison = pd.DataFrame()
df_comparison['sentence_id'] = df_rlb['sentence_id']
df_comparison['source_text'] = df_rlb['source_text']
df_comparison['srl_tags'] = df_rlb['srl_tags']

# add predictions per model to dataframe
pred_rlb = df_rlb['predicted_labels'].tolist()
pred_rlb = [[t[1] for t in inner_list] for inner_list in pred_rlb]
df_comparison['pred_rlb'] = pred_rlb
df_comparison['pred_rlb'] = df_comparison['pred_rlb']

df_comparison['pred_mapping'] = df_mapping['predicted_labels']

df_bert['predicted_labels'] = df_bert['predicted_labels'].apply(format_bert_predictions)
df_bert['pred'] = [[t[1] for t in lst] for lst in df_bert['predicted_labels']]
df_comparison['pred_bert'] = df_bert['pred']

df_legal['predicted_labels'] = df_legal['predicted_labels'].apply(format_bert_predictions)
df_legal['pred'] = [[t[1] for t in lst] for lst in df_legal['predicted_labels']]
df_comparison['pred_legal'] = df_legal['pred']

df_eurlex['predicted_labels'] = df_eurlex['predicted_labels'].apply(format_bert_predictions)
df_eurlex['pred'] = [[t[1] for t in lst] for lst in df_eurlex['predicted_labels']]
df_comparison['pred_eurlex'] = df_eurlex['pred']

df_spanbert['predicted_labels'] = df_spanbert['predicted_labels'].apply(format_bert_predictions)
df_spanbert['pred'] = [[t[1] for t in lst] for lst in df_spanbert['predicted_labels']]
df_comparison['pred_spanbert'] = df_spanbert['pred']

df_multilingual['predicted_labels'] = df_multilingual['predicted_labels'].apply(format_bert_predictions)
df_multilingual['pred'] = [[t[1] for t in lst] for lst in df_multilingual['predicted_labels']]
df_comparison['pred_multilingual'] = df_multilingual['pred']


def print_items_by_index(lists):
    # Assuming all lists have the same length
    list_length = len(lists[0])
    column_names = ['words', 'true', 'rlb', 'mapping', 'bert', 'legal', 'eurlex', 'spanbert', 'multilingual']
    print(' - '.join('{:<13}'.format(column_name) for column_name in column_names))

    for i in range(list_length):
        items = [str(lst[i]) for lst in lists]
        print(' - '.join('{:<13}'.format(item) for item in items))


def compare_predictions(df, sent_index):
    source_text = df.loc[sent_index, 'source_text']
    words = [word for word in source_text.split()]
    true_labels = df.loc[sent_index, 'srl_tags']
    rlb_labels = df.loc[sent_index, 'pred_rlb']
    mapping_labels = df.loc[sent_index, 'pred_mapping']
    bert_labels = df.loc[sent_index, 'pred_bert']
    legal_labels = df.loc[sent_index, 'pred_legal']
    eurlex_labels = df.loc[sent_index, 'pred_eurlex']
    spanbert_labels = df.loc[sent_index, 'pred_spanbert']
    multi_labels = df.loc[sent_index, 'pred_multilingual']
    print(df.loc[sent_index, 'sentence_id'])
    print(source_text)

    if not len(words) == len(true_labels) == len(rlb_labels) == len(mapping_labels) == len(bert_labels) == len(
            legal_labels) == len(eurlex_labels) == len(spanbert_labels) == len(multi_labels):
        print('FALSE')
    else:
        if true_labels == rlb_labels == mapping_labels == bert_labels == legal_labels == eurlex_labels == spanbert_labels == multi_labels:
            print('all models have same predictions')
        else:
            lists = [words, true_labels, rlb_labels, mapping_labels, bert_labels, legal_labels, eurlex_labels,
                     spanbert_labels, multi_labels]
            print_items_by_index(lists)


compare_predictions(df_comparison, 75)
