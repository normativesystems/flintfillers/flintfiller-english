from sklearn.metrics import accuracy_score, balanced_accuracy_score, confusion_matrix, classification_report
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
import itertools
import string


def format_bert_predictions(pred_output):
    # remove CLS and SEP token at start and end of list
    pred_output = pred_output[1:-1]

    # concatenate the tokens that start with ## to their preceding token
    cleaned_pred_output = []
    for label in pred_output:
        label = list(label)
        if not label[0].startswith('##'):
            cleaned_pred_output.append(label)
        else:
            cleaned_pred_output[-1][0] += label[0].strip('##')

    # Map the different usages of arguments
    label_mapping = {'ACTOR': 'Actor',
                     'OBJ': 'Object',
                     'REC': 'Recipient',
                     'V': 'Action',
                     'O': 'O'}

    for label in cleaned_pred_output:
        label[1] = label_mapping[label[1]]

    cleaned_pred_output = [tuple(label) for label in cleaned_pred_output]

    return cleaned_pred_output


def get_accuracy(df):
    mean_accuracy = 0.0
    mean_balanced_accuracy = 0.0
    conf_matrix_list_of_arrays = []
    y_pred_all = []
    y_true_all = []
    for index, row in df.iterrows():
        y_pred = []
        for y in row['predicted_labels']:
            y_pred.append(y[1])

        y_true = row['srl_tags']

        if len(y_true) > -1:
            if len(y_true) < 1 and not len(y_pred) < 1:
                y_true = ['O'] * len(y_pred)
            elif len(y_pred) < 1 and not len(y_true) < 1:
                y_pred = ['O'] * len(y_true)
            elif len(y_pred) < 1 and len(y_true) < 1:
                y_pred = ['O']
                y_true = ['O']

        if len(y_true) != len(y_pred):
            print('y_true', len(y_true), y_true)
            print('y_pred', len(y_pred), y_pred)
            print(len(list(row['source_text'])), row['source_text'])
            continue
        else:
            y_true_no_punc = []
            y_pred_no_punc = []
            for token_true, token_pred in zip(y_true, y_pred):
                if token_true not in string.punctuation:
                    y_true_no_punc.append(token_true)
                    y_pred_no_punc.append(token_pred)
            y_true = y_true_no_punc
            y_pred = y_pred_no_punc
            y_pred_all.extend(y_pred)
            y_true_all.extend(y_true)

        accuracy = accuracy_score(y_true, y_pred)
        balanced_accuracy = balanced_accuracy_score(y_true, y_pred)

        np.set_printoptions(precision=2)

        class_names = ['Actor', 'O', 'Object', 'Recipient', 'Action']
        cm = confusion_matrix(y_true, y_pred, labels=class_names)

        conf_matrix_list_of_arrays.append(cm)
        mean_accuracy += accuracy
        mean_balanced_accuracy += balanced_accuracy

    mean_accuracy /= len(conf_matrix_list_of_arrays)
    mean_balanced_accuracy /= len(conf_matrix_list_of_arrays)

    print('truelen', len(y_true_all))
    print('predlen', len(y_pred_all))
    accuracy_all = accuracy_score(y_true_all, y_pred_all)
    balanced_accuracy_all = balanced_accuracy_score(y_true_all, y_pred_all)
    classification_rep = classification_report(y_true_all, y_pred_all, digits=3)
    print(classification_rep)

    print('ma', mean_accuracy)
    print('mba', mean_balanced_accuracy)
    print('acc all', accuracy_all)
    print('bal acc all', balanced_accuracy_all)

    # cls_accuracy = np.mean(np.array(y_pred_all)[cls_indices] == np.array(y_true_all)[cls_indices])
    # class_accuracy[cls] = cls_accuracy

    # for cls, accuracy in class_accuracy.items():
    #     print("accuracy for class", cls, ':', accuracy)

    return mean_accuracy, mean_balanced_accuracy, conf_matrix_list_of_arrays


def get_accuracy_mapping_model(df):
    mean_accuracy = 0.0
    mean_balanced_accuracy = 0.0
    conf_matrix_list_of_arrays = []
    y_pred_all = []
    y_true_all = []

    for index, row in df.iterrows():
        y_pred = row['predicted_labels']

        y_true = row['srl_tags']

        if len(y_true) > -1:
            if len(y_true) < 1 and not len(y_pred) < 1:
                y_true = ['O'] * len(y_pred)
            elif len(y_pred) < 1 and not len(y_true) < 1:
                y_pred = ['O'] * len(y_true)
            elif len(y_pred) < 1 and len(y_true) < 1:
                y_pred = ['O']
                y_true = ['O']

        if len(y_true) != len(y_pred):
            print('y_true', len(y_true), y_true)
            print('y_pred', len(y_pred), y_pred)
            print(len(list(row['source_text'])), row['source_text'])
            continue
        else:
            y_true_no_punc = []
            y_pred_no_punc = []
            for token_true, token_pred in zip(y_true, y_pred):
                if token_true not in string.punctuation:
                    y_true_no_punc.append(token_true)
                    y_pred_no_punc.append(token_pred)
            y_true = y_true_no_punc
            y_pred = y_pred_no_punc
            y_pred_all.extend(y_pred)
            y_true_all.extend(y_true)

        accuracy = accuracy_score(y_true, y_pred)
        balanced_accuracy = balanced_accuracy_score(y_true, y_pred)

        np.set_printoptions(precision=2)

        class_names = ['Actor', 'O', 'Object', 'Recipient', 'Action']
        cm = confusion_matrix(y_true, y_pred, labels=class_names)

        conf_matrix_list_of_arrays.append(cm)
        mean_accuracy += accuracy
        mean_balanced_accuracy += balanced_accuracy

    mean_accuracy /= len(conf_matrix_list_of_arrays)
    mean_balanced_accuracy /= len(conf_matrix_list_of_arrays)

    print('truelen', len(y_true_all))
    print('predlen', len(y_pred_all))
    accuracy_all = accuracy_score(y_true_all, y_pred_all)
    balanced_accuracy_all = balanced_accuracy_score(y_true_all, y_pred_all)
    classification_rep = classification_report(y_true_all, y_pred_all, digits=3)
    print(classification_rep)

    print('ma', mean_accuracy)
    print('mba', mean_balanced_accuracy)
    print('acc all', accuracy_all)
    print('bal acc all', balanced_accuracy_all)

    # SHOW CONFUSION MATRIX

    return mean_accuracy, mean_balanced_accuracy, conf_matrix_list_of_arrays


def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print('normalized cm')
    else:
        print('cm without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def show_confusion_matrix(conf_matrix_list_of_arrays, class_names):
    mean_of_conf_matrix_arrays = np.mean(conf_matrix_list_of_arrays, axis=0)
    print(mean_of_conf_matrix_arrays.tolist())
    plt.figure()
    plot_confusion_matrix(mean_of_conf_matrix_arrays, classes=class_names, title='Mean confusion matrix',
                          normalize=True)

    plt.show()


def show_confusion_matrix_all(cm_all, class_names):
    plt.figure()
    plot_confusion_matrix(cm_all, classes=class_names, title='ALL', normalize=True)
    plt.show()


# getting accuracy scores and confusion matrix for bert finetuned models
df_bert = pd.read_csv('../../src/resource/labeled_results/multilingual_predictions.csv',
                      converters={'srl_tags': eval, 'predicted_labels': eval})
df_bert['predicted_labels'] = df_bert['predicted_labels'].apply(format_bert_predictions)
ma, mba, cma = get_accuracy(df_bert)
# show_confusion_matrix(cma, ['Actor', 'O', 'Object', 'Recipient', 'Action'])


# getting accuracy scores and confusion matrix for rulebased model
# df_rlb = pd.read_csv('../../src/resource/labeled_results/rulebased_predictions.csv', converters={'srl_tags': eval, 'predicted_labels': eval})
# ma, mba, cma = get_accuracy(df_rlb)
# show_confusion_matrix(cma, ['Actor', 'O', 'Object', 'Recipient', 'Action'])

# getting accuracy scores and confusion matrix for mapping model
# df_mapping = pd.read_csv('../../src/resource/labeled_results/mapping_predictions.csv', converters={'srl_tags': eval, 'predicted_labels': eval})
#
# ma, mba, cma = get_accuracy_mapping_model(df_mapping)
# show_confusion_matrix(cma, ['Actor', 'O', 'Object', 'Recipient', 'Action'])
