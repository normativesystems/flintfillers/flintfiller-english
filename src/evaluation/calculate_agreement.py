import numpy as np
import pandas as pd
import pathlib

directory = pathlib.Path(__file__).parent.parent
path = directory / 'resource' /'data' / 'processed_annotations' / 'all_annotations.csv'
annotations = pd.read_csv(path, converters={'tokens': eval, 'srl_tags': eval})
def drop_incomplete_annotations(annotations, no_of_annotations):
    """
    Set a number of annotations each sentence_id must have. If the actual number of annotations for a sentence_id
    does not match this number, then we drop the sentences from the set to calculate agreement on.

    Parameters:
        annotations
        no_of_annotations

    Returns:
        annotations
    """
    sentence_id_counts = annotations['sentence_id'].value_counts().rename_axis('sentence_id').reset_index(name='counts')
    print(len(sentence_id_counts), 'sentences have been annotated')

    # the following sentence ids have been annotated n times (n = no_of_annotations)
    sentence_ids = list(sentence_id_counts.loc[sentence_id_counts['counts'] == no_of_annotations]['sentence_id'])
    print(len(sentence_ids), 'of those sentences have been annotated', no_of_annotations, 'times')

    # remove sentence ids where number of tokens is not equal to number of srl_tags  #todo figure out why this happens
    for index, row in annotations.iterrows():
        if len(row.tokens) != len(row.srl_tags):
            if row.sentence_id in sentence_ids:
                sentence_ids.remove(row.sentence_id)
    print(len(sentence_ids), 'of those sentence are in the right shape for calculating inter-annotator agreement')

    # select a subset of the annotations using the sentence_ids that have been annotated n times
    annotations = annotations.loc[annotations['sentence_id'].isin(sentence_ids)]
    print(annotations.columns)

    return annotations

def preprocess_annotations(annotations):
    preprocessed_annotations = pd.DataFrame()

    for sentence_id in annotations['sentence_id'].unique():
        annotations_per_id = annotations.loc[annotations['sentence_id'] == sentence_id]
        x = 1                                                   # ith annotation of a certain sentence id

        # for annotations with the same sent ID, create a df with tokens on a separate line and the annotations in cols
        annotations_for_sent_id = pd.DataFrame()
        for index, row in annotations_per_id.iterrows():
            matrix = np.column_stack((row.tokens, row.srl_tags))
            if annotations_for_sent_id.empty:
                annotations_for_sent_id = pd.DataFrame(matrix, columns=['token', 'srl_tag'+str(x)])
                annotations_for_sent_id['sentence_id'] = sentence_id
            else:
                temp_df = pd.DataFrame(matrix, columns=['token', 'srl_tag'+str(x)])
                temp_df['sentence_id'] = sentence_id
                annotations_for_sent_id = pd.merge(annotations_for_sent_id, temp_df, left_index=True, right_index=True)

            x += 1

        # create V, ACTOR, OBJ, REC and O columns to collect the counts
        # note: interpunction doesn't have a tag so we drop it
        annotations_for_sent_id['V'], annotations_for_sent_id['ACTOR'], annotations_for_sent_id['OBJ'], \
        annotations_for_sent_id['REC'], annotations_for_sent_id['O'] = [0, 0, 0, 0, 0]

        # collect the counts
        for srl_tag_column in ['srl_tag'+str(i) for i in range(1, x)]:
            annotations_for_sent_id.loc[annotations_for_sent_id[srl_tag_column] == 'Action', 'V'] += 1
            annotations_for_sent_id.loc[annotations_for_sent_id[srl_tag_column] == 'Actor', 'ACTOR'] += 1
            annotations_for_sent_id.loc[annotations_for_sent_id[srl_tag_column] == 'Object', 'OBJ'] += 1
            annotations_for_sent_id.loc[annotations_for_sent_id[srl_tag_column] == 'Recipient', 'REC'] += 1
            annotations_for_sent_id.loc[annotations_for_sent_id[srl_tag_column] == 'O', 'O'] += 1

        preprocessed_annotations = pd.concat([preprocessed_annotations, annotations_for_sent_id], ignore_index=True)

    return preprocessed_annotations

def format_annotations(preprocessed_annotations):
    """
    Takes the preprocessed annotations dataframe and creates a matrix containing only relevant columns

    Args:
        preprocessed_annotations:

    Returns:
        preprocessed_annotations_matrix
    """

    # drop all columns except the columns with counts for each tag
    preprocessed_annotations = preprocessed_annotations[['V', 'ACTOR', 'OBJ', 'REC', 'O']]

    # drop all rows without annotation (this is punctuation, for example)
    preprocessed_annotations = preprocessed_annotations.loc[~(preprocessed_annotations == 0).all(axis=1)]

    # convert from df to matrix
    preprocessed_annotations_matrix = preprocessed_annotations.to_numpy()

    return preprocessed_annotations_matrix

def calculate_fleiss_kappa(matrix):
    """Computes Fleiss' kappa for group of annotators.
    :param matrix: a matrix of shape (:attr:'n', :attr:'k') with 'n' = number of subjects and 'k' = the number of categories.
        'M[i, j]' represent the number of raters who assigned the 'i'th subject to the 'j'th category.
    :type: numpy matrix
    :rtype: float
    :return: Fleiss' kappa score
    """
    n, k = matrix.shape  # n is # of items, k is # of categories
    n_annotators = float(np.sum(matrix[0, :]))  # # of annotators
    tot_annotations = n * n_annotators  # the total # of annotations
    category_sum = np.sum(matrix, axis=0)  # the sum of each category over all items

    # chance agreement
    p = category_sum / tot_annotations  # the distribution of each category over all annotations
    pbarE = np.sum(p * p)  # average chance agreement over all categories

    # observed agreement
    P = (np.sum(matrix * matrix, axis=1) - n_annotators) / (n_annotators * (n_annotators - 1))
    Pbar = np.sum(P) / n  # add all observed agreement chances per item and divide by amount of items

    return round((Pbar - pbarE) / (1 - pbarE), 3)

def format_annotations_specific_agreement(preprocessed_annotations):
    """
    Takes the preprocessed annotations dataframe and creates a matrix containing only relevant columns

    Args:
        preprocessed_annotations:

    Returns:
        preprocessed_annotations_matrix
    """

    # drop all columns except the columns with counts for each tag
    preprocessed_annotations = preprocessed_annotations[['V', 'ACTOR', 'OBJ', 'REC']]

    # drop all rows without annotation (this is punctuation, for example)
    preprocessed_annotations = preprocessed_annotations.loc[~(preprocessed_annotations == 0).all(axis=1)]

    # convert from df to matrix
    preprocessed_annotations_matrix = preprocessed_annotations.to_numpy()

    return preprocessed_annotations_matrix

def calculate_specific_agreement(matrix):
    """
    Computes specific agreement per category.
    :param matrix: a matrix of shape (:attr: 'n', :attr:'k') with 'n' = number of subjects and 'k' = the number of categories.
    'r[i,j]' represents the number of raters who assigned the ith subject with the jth class.
    :return: the specific agreement score for each category
    """
    n, k = matrix.shape
    n_annotators = float(np.sum(matrix[0, :]))
    teller_sum = np.sum(matrix * (matrix - 1), axis=0)
    noemer_sum = np.sum(matrix * (n_annotators - 1), axis=0)
    sa_per_class = teller_sum / noemer_sum

    print('Specific agreement V:', round(sa_per_class[0],3))
    print('Specific agreement ACTOR:', round(sa_per_class[1],3))
    print('Specific agreement OBJ:', round(sa_per_class[2],3))
    print('Specific agreement REC:', round(sa_per_class[3],3))
    # print('Specific agreement O:', round(sa_per_class[4],3))


annotations = drop_incomplete_annotations(annotations,5)
preprocessed_annotations = preprocess_annotations(annotations)
print(preprocessed_annotations)
preprocessed_annotations_matrix = format_annotations(preprocessed_annotations)
print(preprocessed_annotations_matrix)

kappa = calculate_fleiss_kappa(preprocessed_annotations_matrix)
print('kappa:',kappa)

preprocessed_annotations_matrix_specific_agreement = format_annotations_specific_agreement(preprocessed_annotations)
print(preprocessed_annotations_matrix_specific_agreement)
specific_agreement = calculate_specific_agreement(preprocessed_annotations_matrix)
print(specific_agreement)
