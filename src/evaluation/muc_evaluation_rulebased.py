from nervaluate import Evaluator
import pandas as pd
import ast

def convert_to_bio_tags(tags):
    bio_tags = []
    prev_tag = 'O'
    for tag in tags:
        if tag == 'O':
            bio_tags.append(tag)
            prev_tag = 'O'
        else:
            if tag == prev_tag:
                bio_tags.append('I-' + tag)
            else:
                bio_tags.append('B-' + tag)
            prev_tag = tag
    return bio_tags

def get_muc_scores(true, pred):
    # replace normal tags with BIO-tags
    for i in range(len(true)):
        true[i] = convert_to_bio_tags(true[i])
        pred[i] = convert_to_bio_tags(pred[i])

    # get the MUC scores
    evaluator = Evaluator(true, pred, tags=['Actor', 'Action', 'Object', 'Recipient'], loader='list')
    results, results_per_tag = evaluator.evaluate()
    return results, results_per_tag

df_rulebased_predictions = pd.read_csv("../resource/labeled_results/rulebased_predictions.csv", delimiter=',', converters={'predicted_labels': eval})

true_labels = df_rulebased_predictions['srl_tags'].tolist()
true_labels = [ast.literal_eval(s) for s in true_labels]
true_labels = [[word.replace('.', 'O').replace(',', 'O').replace(';', 'O').replace(':', 'O').replace('(', 'O').replace(')', 'O').replace('-', 'O').replace('/', 'O') for word in sublist] for sublist in true_labels]

pred_labels = df_rulebased_predictions['predicted_labels'].tolist()
pred_labels = [[t[1] for t in inner_list] for inner_list in pred_labels]

results, results_per_tag = get_muc_scores(true_labels, pred_labels)
print(results_per_tag)