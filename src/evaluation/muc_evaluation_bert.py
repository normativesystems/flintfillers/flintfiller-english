import pprint
from nervaluate import Evaluator
import pandas as pd
import ast

from src.evaluation.metrics import format_bert_predictions

pred_labels = pd.read_csv("../resource/labeled_results/multilingual_predictions.csv", delimiter=',',
                          converters={'predicted_labels': eval})
pred_labels = pred_labels.rename(columns={'predicted_labels': 'predicted_srl_tags'})

# do formatting on pred_labels (remove all BERT-specific tokens)
pred_labels['predicted_srl_tags'] = pred_labels['predicted_srl_tags'].apply(format_bert_predictions)
pred_labels['predicted_list'] = [[t[1] for t in lst] for lst in pred_labels['predicted_srl_tags']]

true_labels = pred_labels['srl_tags'].tolist()
true_labels = [ast.literal_eval(s) for s in true_labels]
true_labels = [[word.replace('.', 'O').replace(',', 'O').replace(';', 'O').replace(':', 'O').replace('(', 'O').replace(
    ')', 'O').replace('-', 'O').replace('/', 'O') for word in sublist] for sublist in true_labels]
# print(true_labels)
pred_labels = pred_labels['predicted_list'].tolist()


# print(pred_labels)

def convert_to_bio_tags(tags):
    bio_tags = []
    prev_tag = 'O'
    for tag in tags:
        if tag == 'O':
            bio_tags.append(tag)
            prev_tag = 'O'
        else:
            if tag == prev_tag:
                bio_tags.append('I-' + tag)
            else:
                bio_tags.append('B-' + tag)
            prev_tag = tag
    return bio_tags


def get_muc_scores(true, pred):
    # replace normal tags with BIO-tags
    for i in range(len(true)):
        true[i] = convert_to_bio_tags(true[i])
        pred[i] = convert_to_bio_tags(pred[i])

    # get the MUC scores
    evaluator = Evaluator(true, pred, tags=['Actor', 'Action', 'Object', 'Recipient'], loader='list')
    results, results_per_tag = evaluator.evaluate()
    return results, results_per_tag


# true = [
#     ['O', 'O', 'Actor', 'Actor', 'O'],
#     ['O', 'Recipient', 'LOC', 'LOC', 'LOC', 'O'],
# ]
#
# pred = [
#     ['O', 'O', 'PER', 'PER', 'O'],
#     ['O', 'LOC', 'LOC', 'LOC', 'LOC', 'O'],
# ]

results, results_per_tag = get_muc_scores(true_labels, pred_labels)
pprint.pp(results_per_tag)
pprint.pp(results)
