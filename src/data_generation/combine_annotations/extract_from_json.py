import json
import csv

# open input and output files
with open('annotations_full.json', 'r', encoding='utf-8') as f_input, open(
        '../../resource/data/unprocessed_annotations/final_annotations_nl_en.csv', 'w', newline='') as f_output:

    # create csv writer object
    writer = csv.writer(f_output)

    # iterate over each line in input file
    for line in f_input:

        # load json object from line
        json_object = json.loads(line)

        # extract nested source object
        source_object = json_object['_source']

        # get keys for csv header row
        keys = source_object.keys()

        # write header row if it hasn't been written yet
        if not writer.writerow:
            writer.writerow(keys)

        # write row of values for source object
        row_values = [f"'{value}'" if key == 'term' else value for key, value in source_object.items()]
        writer.writerow(source_object.values())



