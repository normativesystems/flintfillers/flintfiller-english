import argparse
import pathlib
import pandas as pd
import re

directory = pathlib.Path(__file__).parent.parent.parent
def remove_dutch_annotations(annotation_file: str):
    """
    Function that takes the file containing all unprocessed_annotations from the annotation tool and returns a dataframe
    with only the unprocessed_annotations for the English law texts.
    """
    df = pd.read_csv(annotation_file, encoding='latin-1', usecols=[1, 2, 3, 4, 5, 6, 7], quotechar='"')
    df.columns = ['term', 'tag', 'start', 'end', 'sentence_id', 'user_id', 'comments']

    mask = df['user_id'].isin(
        ['annotator_1', 'annotator_2', 'annotator_3', 'annotator_4', 'annotator_6', 'annotator_7',
         'annotator_5'])
    df = df[mask]

    return df

def get_user_dfs(english_annotations):
    """
    Takes a df with all unprocessed_annotations on english sentences.
    Returns a list of tuples (user_id, dataframe) with the user_id and a dataframe with the unprocessed_annotations of each
    annotator.
    """
    df = english_annotations
    df_user_sorted = df.sort_values('user_id')
    dfs_by_user = {}
    groups = df_user_sorted.groupby('user_id')

    for user_id, group in groups:
        dfs_by_user[user_id] = group

    all_user_dfs_list = []

    all_user_dfs_list.append(('annotator_3',dfs_by_user['annotator_3']))
    all_user_dfs_list.append(('annotator_2', dfs_by_user['annotator_2']))
    all_user_dfs_list.append(('annotator_1', dfs_by_user['annotator_1']))
    all_user_dfs_list.append(('annotator_4', dfs_by_user['annotator_4']))
    # all_user_dfs_list.append(('annotator_7', dfs_by_user['annotator_7']))
    all_user_dfs_list.append(('annotator_5', dfs_by_user['annotator_5']))
    # all_user_dfs_list.append(('annotator_6', dfs_by_user['annotator_6']))

    return all_user_dfs_list

# function that creates new dataframe with only sentences that were annotated by this annotator
def get_relevant_sentences(df_annotator):
    """
    Function creates new dataframe that only contains sentences that were annotated by specific annotator.
    """

    # read the relevant files
    file_path = directory / 'resource' / 'data' / 'en_sents.csv'
    df_default = pd.read_csv(file_path)

    # find the unique sentences annotated by this annotator
    annotated_sentences = list(df_annotator['sentence_id'].unique())

    # create df with only the sentences that were annotated by the annotator
    df_annotated_sents = df_default[df_default.sentence_id.isin(annotated_sentences)]

    return df_annotated_sents

def get_intermediary_tag(token):
    """"
    Returns an intermediary tag per token
    """
    if token in {'.', ',', '!', '?', ':', ';'}:
        return token
    else:
        return 'O'

def add_tokens_tags_to_df(dataframe, user_id):
    """
    Adds tokens and srl_tags columns and values to the annotated sentences dataframe
    """
    dataframe['tokens'] = dataframe.apply(lambda row: row['source_text'].strip().split(), axis=1)
    dataframe['srl_tags'] = dataframe['tokens'].apply(lambda tokens: [get_intermediary_tag(token) for token in tokens])
    dataframe['user_id'] = user_id
    return dataframe

def replace_tags(string, tokens, tags, substring, start_index, end_index, tag):
    """
    Function translates the tag from an annotation to where it should be in the tag list and
    returns the tag list so far
    """
    # Find the corresponding start and end token indices
    start_token_idx = None
    end_token_idx = None
    current_idx = 0
    for i, token in enumerate(tokens):
        if current_idx == start_index:
            start_token_idx = i
        if current_idx >= end_index:
            end_token_idx = i
            break
        current_idx += len(token) + 1  # add 1 for the space between tokens

    # Replace the tags of the corresponding tokens with the given tag
    if start_token_idx is not None and end_token_idx is not None:
        for i in range(start_token_idx, end_token_idx):
            tags[i] = tag

    return tags

def update_tags(df_annotations, df_total):
    """
    Function that reads each annotation in the unprocessed_annotations dataframe and
    updates the tags in the total dataframe.
    """

    for _, row in df_annotations.iterrows():
        sentence_id = row['sentence_id']
        substring = row['term']
        start_index = row['start'] - 1
        end_index = row['end']
        tag = row['tag']
        user_id = row['user_id']

        # Find the corresponding row in df1
        updated_row = df_total[df_total['sentence_id'] == sentence_id].iloc[0]

        # Call the replace_tags function to update the srl_tags value in df1_row
        updated_row['srl_tags'] = replace_tags(
            string=updated_row['source_text'],
            tokens=updated_row['tokens'],
            tags=updated_row['srl_tags'],
            substring=substring,
            start_index=start_index,
            end_index=end_index,
            tag=tag,
        )

        df_total.loc[df_total['sentence_id'] == sentence_id]['srl_tags'].iloc[0] = updated_row['srl_tags']

    return df_total

def split_tokens_and_tags(tokens, tags):
    """In the dataframe's tokens column the function finds all special characters that are not in a len=1 token
    and splits them. In the matching srl_tags column row it adds a tag corresponding to the old token in the correct location"""
    special_char_regex = re.compile(r'([\W_])')
    new_tokens = []
    new_tags = []
    for i, token in enumerate(tokens):
        # Split token if it contains special characters
        token = token.replace('"', '').replace("'", '')
        if special_char_regex.search(token):
            subtokens = special_char_regex.split(token)
            subtokens = [token for token in subtokens if token != '']
            token_tags = []
            for token in subtokens:
                new_tokens.append(token)
                token_tags.append(tags[i])
            for tag in token_tags:
                new_tags.append(tag)
        else:
            new_tokens.append(token)
            new_tags.append(tags[i])

    return new_tokens, new_tags

def split_tokens_dataframe(df):
    df[['tokens', 'srl_tags']] = df.apply(lambda row: split_tokens_and_tags(row['tokens'], row['srl_tags']), axis=1,result_type='expand')
    return df

def get_user_annotations(user_id,user_df):
    """Get dataframe with all annotated sentences for a single user"""
    df_user_sents = get_relevant_sentences(user_df)
    df_default_tags = add_tokens_tags_to_df(df_user_sents, user_id)
    df_updated_tags = update_tags(user_df,df_default_tags)
    df_updated_tags = df_updated_tags.iloc[:, 1:]
    df_updated_tags = df_updated_tags.reindex(columns=['user_id', 'sentence_id', 'source_text', 'tokens', 'srl_tags'])
    df_updated_split = split_tokens_dataframe(df_updated_tags)

    return df_updated_split


# file_path = directory / 'resource' / 'unprocessed_annotations' / 'all_intermediate_annotations_nl_en.csv'
# english_annotations = remove_dutch_annotations(file_path)
# df_list = get_user_dfs(english_annotations)
#
# updated_dfs = []
# for user, df in df_list:
#     user_annotated_sents = get_user_annotations(user,df)
#     updated_dfs.append(user_annotated_sents)
#
# combined_df = pd.concat(updated_dfs).reset_index(drop=True)


def main():
    input_args = parse_commandline_arguments()

    english_annotations = remove_dutch_annotations(input_args.dataset)
    df_list = get_user_dfs(english_annotations)
    updated_df_list = []

    for user,df in df_list:
        user_annotated_sents = get_user_annotations(user,df)
        updated_df_list.append(user_annotated_sents)

    combined_df = pd.concat(updated_df_list).reset_index(drop=True)

    combined_df.to_csv(input_args.output, index=False, encoding='utf-8')

def parse_commandline_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--dataset',
                        help="location of dataset with all unprocessed_annotations")
    parser.add_argument('-o', '--output',
                        help="location and name of output csv file")

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()