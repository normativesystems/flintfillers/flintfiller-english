import pandas as pd
import argparse

def filter_sentences(df):
    # get the sentences that have been annotated once and add annotation to new dataframe
    unique_ids = df['sentence_id'].value_counts() == 1
    unique_ids = unique_ids[unique_ids].index
    df_unique = df[df['sentence_id'].isin(unique_ids)]

    # get the sentences that have been annotated multiple times
    # randomly pick one annotation per sentence to add to new dataframe
    multi_ids = df['sentence_id'].value_counts() > 1
    multi_ids = multi_ids[multi_ids].index
    df_multi = df[df['sentence_id'].isin(multi_ids)]
    df_multi_filtered = df_multi.groupby('sentence_id').apply(lambda x: x.sample(1)).reset_index(drop=True)

    # create new final df
    final_df = pd.concat([df_unique, df_multi_filtered], ignore_index=True)

    return final_df

def main():
    input_args = parse_commandline_arguments()

    tagged_file = input_args.data
    df = pd.read_csv(tagged_file)
    final_df = filter_sentences(df)

    final_df.to_csv(input_args.output, index=False, encoding='utf-8')

def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data',
                        help="path to annotated data")
    parser.add_argument('-o', '--output',
                        help="path to filtered final dataset")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()